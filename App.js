import React, { Component } from 'react';
import { Provider } from 'react-redux';
// import Navigator from './src/Navigation/Router';
import SwitchNavigator from './src/Navigation/SwitchNavigator';
import { NetInfo, AsyncStorage } from 'react-native';
import NavigationService from './utils/NavigationService';

import store from './store';
import _Modal from './src/Components/Modal';
import { CHECK_CONNECTION } from "./actions/types";
import { mountInventory, createTransaction } from "./actions/equipmentAction";

export default class App extends Component{

  constructor() {
    super();
    this.count = 0;
  }

  state = {
    lostConnection: false,
    getConnection: false

  };

  componentDidMount() {
    NetInfo.addEventListener('connectionChange', this.test);
    NetInfo.addEventListener('connectionChange', this.delayRequest);
  };

  delayRequest = (e) => {
    if(e.type !== 'none') {
      AsyncStorage.getItem('transaction')
        .then(data => {
          if (data) {
            data = JSON.parse(data);
            data.map(item => {
              if (item.type === 'mount') {
                store.dispatch(mountInventory(item.equipmentId, item.inventoryId)) // mount inventory to equipment
                  .then(() => {
                    store.dispatch(createTransaction(item.equipmentId, item.inventoryId, item.statusId)) //   create transaction for mounting inventory
                      .catch((err) => {
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    console.log(err);
                  });
              } else if (item.type === 'unmount') {
                store.dispatch(createTransaction(item.equipmentId, item.inventoryId, item.statusId)) //   create transaction for mounting inventory
                  .catch((err) => {
                    console.log(err);
                  });
              }
            });
            AsyncStorage.removeItem('transaction');
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  test = (e) => {
    if(e.type === 'none') {
      this.setState({
        lostConnection: true
      });
      store.dispatch({
        type: CHECK_CONNECTION,
        payload: false
      });
    } else if(this.count > 0){
      this.setState({
        getConnection: true
      });
      store.dispatch({
        type: CHECK_CONNECTION,
        payload: true
      });
    }
    this.count ++;
  };

  closeModal = () => {
    this.setState({
      lostConnection: false,
      getConnection: false
    });
  };

  render() {

    return(
      <React.Fragment>
        <_Modal
          closeModal={this.closeModal}
          visible={this.state.lostConnection}
          type={'alert'}
        >
          Проверьте соединение с интернетом!
        </_Modal>
        <_Modal
          closeModal={this.closeModal}
          visible={this.state.getConnection}
          type={'alert'}
        >
          Соединение с интернетом установлено!
        </_Modal>
        <Provider store={store} >
          <SwitchNavigator ref={navigatorRef => NavigationService.setTopLevelNavigator(navigatorRef)}/>
        </Provider>
      </React.Fragment>
    )
  }
};

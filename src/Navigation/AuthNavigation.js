import { createAppContainer, createStackNavigator } from 'react-navigation';
import RegisterScreen from '../Views/RagisterPage';
import AuthCamera from '../Views/AuthCamera';

const AuthNavigation = createStackNavigator(
  {
    RegisterScreen,
    AuthCamera
  },
  {
    headerMode: 'none'
  },
  {
    initialRouteName: RegisterScreen,
    // navigationOptions: { header: null },
  },
);

export default createAppContainer(AuthNavigation);

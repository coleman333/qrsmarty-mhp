import React, { Component } from 'react';
import { View } from 'react-native';

export default class Viewfinder extends Component {
    constructor(props) {
        super(props);

        this.getBackgroundColor = this.getBackgroundColor.bind(this);
        this.getSizeStyles = this.getSizeStyles.bind(this);
        this.getEdgeSizeStyles = this.getEdgeSizeStyles.bind(this);
    }

    getBackgroundColor() {
        return ({
            backgroundColor: this.props.backgroundColor,
        });
    }

    getSizeStyles() {
        return ({
            height: this.props.height,
            width: this.props.width,
        });
    }

    getEdgeSizeStyles() {
        return ({
            height: this.props.borderLength,
            width: this.props.borderLength,
        });
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    borderColor: "rgba(0,0,0,.7)",
                    borderWidth: 60,
                    borderTopWidth: 180,
                    borderBottomWidth: 180
                }}>
            </View>
        );
    }
}

Viewfinder.defaultProps = {
    backgroundColor: 'transparent',
    borderWidth: 2,
    borderLength: 30,
    color: 'white',
    height: 200,
    isLoading: false,
    width: 200,
};

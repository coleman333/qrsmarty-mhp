import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, AsyncStorage } from 'react-native';
import Camera from 'react-native-camera';//  todo: maybe change library to new
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';

import styles from './styles';
import ViewFinder from './ViewFinder';
import _Modal from '../../Components/Modal';
import {  bindInventoryToSharpening, checkWarehouse } from '../../../actions/inventoryAction';
import { registrationAction } from '../../../actions/AuthAction';
import { closeError } from '../../../actions/errorAction';
// import { BackHandler } from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
// import Button from '../Button';
import Colors from "../../Constants/Colors";
import Button from "../../Components/Qrscaner";

// import Transaction from "../Transaction";

export class AuthCamera extends Component {

  state = {
    stock: true,
    stockId: null,
    nextWindow: false,
    stockError: false,
    bindError: false,
    successModal: false,
    scan: true,
    mac: null
  };

  // async componentDidMount() {
  //
  //
  // }

  componentWillUnmount() { }

  async componentWillReceiveProps( nextProps ) {
    if(nextProps.token){
      console.log('nextProps.token :::;;;;;;;; ', nextProps.token);
      this.setState({token: nextProps.token});
      await AsyncStorage.setItem('token', nextProps.token);
      this.setState( {
        successModal: true,
        scan: false
      } );

    }
    if(nextProps.registrationError){
      this.setState({registrationError:true})
    }
  };

  registration = ( user_id ) => {
    this.setState( { user_id: user_id } );
    this.props.registrationAction( { code: user_id });
  };

  resolveSuccessModal = () => {
    this.setState( {
      successModal: false,
      scan: false

    } );
    this.props.navigation.navigate('mainNavigator')

  };

  openNextWindow = () => {
    this.setState( {
      nextWindow: true,
      scan: false
    } );
  };

  resolveNextWindow = () => {
    this.setState( {
      nextWindow: false,
      scan: true
    } );
  };

  rejectNextWindow = () => {
    // this.goBack();
    this.props.navigation.navigate( 'Identification' );
    this.setState( {
      nextWindow: false,
      scan: true,
      stock: true
    } );
  };

  closeModal = () => {
    this.props.closeError();
    this.setState( {
      scan: true,          //true
      registrationError: false
    } );
  };

  turnFlash = () => {
    if ( this.state.torch === Camera.constants.TorchMode.on ) {
      this.setState( { torch: Camera.constants.TorchMode.off, flash: Camera.constants.FlashMode.off } )
    } else {
      this.setState( { torch: Camera.constants.TorchMode.on, flash: Camera.constants.FlashMode.on } )
    }
  }

  render() {
    let { flash, torch } = this.state;

    let page;
    if ( this.props.navigation.state ) {
      page = this.props.navigation.state.routeName;
    }

    return (
      <View
        style={ { flex: 1 } }
      >
        <_Modal
          closeModal={ this.closeModal }
          visible={ this.state.registrationError }
          type={ 'alert' }
        >
         ошибка регистрации
        </_Modal>
        <_Modal
          closeModal={ this.rejectNextWindow }
          confirmModal={ this.resolveNextWindow }
          visible={ this.state.nextWindow }
          type={ 'confirm' }
        >
          { this.state.page && this.state.page !== 'Sharpening' ?
            'Операция Успешна!\r\nХотите ли принять другую деталь на склад?' :
            'Операция Успешна!\r\nХотите ли принять другую деталь на участок заточки?'
          }
        </_Modal>
        <_Modal
          closeModal={ this.resolveSuccessModal }
          visible={ this.state.successModal }
          type={ 'success' }
        >
          Регистрация прошла успешно
        </_Modal>

        <Camera
          // flashMode = {this.state.flash}
          torchMode={ this.state.torch }
          style={ { flex: 1 } }
          onBarCodeRead = { ( { data } ) => {
            if ( this.state.scan ) {
            this.setState( { scan: false } );
              this.registration( data );
            }
          } }
        >
          { Platform.OS === 'ios' && <ViewFinder/> }

          <React.Fragment>
            <View style={ styles.lightButton }>
              <TouchableOpacity onPress={ this.turnFlash } style={ styles.stockButton }>
                <Text style={ styles.stockButtonStyle }> освещение </Text>
              </TouchableOpacity>
            </View>
            { Platform.OS === 'android' && <ViewFinder/> }
            <View style={ styles.header }>
              <Text style={ styles.headerText }> Отсканируйте QR-код для входа в систему </Text>
            </View>
          </React.Fragment>


        </Camera>
      </View>
    )
  }
}

const mapStateToProps = ( state ) => ({
  registrationError: state.error.errors.registrationError,
  checkWarehouseError: state.error.errors.authorizationError,
  token: state.auth.token,

  // checkWarehouseResult: state.inventory.checkWarehouseResult
});

export default connect( mapStateToProps, {
  checkWarehouse, bindInventoryToSharpening, closeError, registrationAction } )( AuthCamera )
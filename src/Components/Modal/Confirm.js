import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Svg, { G, Path } from 'react-native-svg';

import PropTypes from 'prop-types';
import Button from '../Button';
import Colors from '../../Constants/Colors';
import styles from './styles';

export default class Confirm extends Component {
  constructor (props) {
    super(props);
    this.confirm = this.confirm.bind(this);
    this.cancel = this.cancel.bind(this);
  }

  confirm () {
    const { onClick } = this.props;
    onClick();
  }

  cancel () {
    const { onCancel } = this.props;
    onCancel();
  }

  render () {
    const { children } = this.props;
    return (
      <View
        style={styles.confirm}
      >
        <View
          style={styles.imageBlock}
        >
          <Svg viewBox="0 0 52 52" width="90" height="90">
            <G>
              <Path d="M26,0C11.664,0,0,11.663,0,26s11.664,26,26,26s26-11.663,26-26S40.336,0,26,0z M26,50C12.767,50,2,39.233,2,26   S12.767,2,26,2s24,10.767,24,24S39.233,50,26,50z" fill="#e47b7b" />
              <Path d="M26,10c-0.552,0-1,0.447-1,1v22c0,0.553,0.448,1,1,1s1-0.447,1-1V11C27,10.447,26.552,10,26,10z" fill="#e47b7b" />
              <Path d="M26,37c-0.552,0-1,0.447-1,1v2c0,0.553,0.448,1,1,1s1-0.447,1-1v-2C27,37.447,26.552,37,26,37z" fill="#e47b7b" />
            </G>
          </Svg>
        </View>
        <View
          style={styles.textBlock}
        >
          <Text
            style={styles.text}
          >
            {children}
          </Text>
        </View>
        <View
          style={styles.buttonBlock}
        >
          <Button
            onClick={this.confirm}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.success
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            ДА
          </Button>
          <Button
            onClick={this.cancel}
            buttonStyle={{
              width: '30%',
              backgroundColor: Colors.red
            }}
            textStyle={{
              textAlign: 'center',
              color: Colors.white
            }}
          >
            НЕТ
          </Button>
        </View>
      </View>
    );
  }
}

Confirm.defaultProps = {
  onClick () {

  },
  onCancel () {

  },
  children: ''
};

Confirm.propTypes = {
  onClick: PropTypes.func,
  onCancel: PropTypes.func,
  children: PropTypes.string
};

import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    position: 'absolute',
    zIndex: 1
  },
  buttonsContainer: {
    // flex: 1,
    flexDirection: 'column',
    // justifyContent: 'space-around',
    width: '100%',
    alignItems: 'center',
    position: 'absolute',
    bottom: 10,
    // paddingBottom: 10

  },
  buttonContainer:{
    paddingBottom: '1%',
    width: '60%'
  },
  headerText: {
    fontSize: 24,
    color: '#fff',
    textAlign: 'center',
    padding: 30,
    margin: 'auto'
  }
});

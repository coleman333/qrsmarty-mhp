import { StyleSheet } from 'react-native';

import Colors from '../../Constants/Colors';

export default StyleSheet.create({
  button: {
    paddingHorizontal: 10,
    backgroundColor: Colors.mhp_blue,
    height: 38,
    borderRadius: 20,
    justifyContent: 'center'
  },
  textButton: {
    textAlign: 'center',
    color: Colors.white
  }
});

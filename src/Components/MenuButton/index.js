import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import PropTypes from 'prop-types';
import Colors from '../../Constants/Colors';

import styles from './styles';

export const MenuButton = (props) => {
  const { onClick } = props;
  return (
    <TouchableOpacity
      style={styles.menuButton}
      onPress={() => {
        onClick();
      }}
    >
      <Icon
        name="menu"
        // name="bars"
        size={30}
        color={Colors.gray}
      />
    </TouchableOpacity>
  );
};

export default MenuButton;

MenuButton.defaultProps = {
  onClick () {

  }
};

MenuButton.propTypes = {
  onClick: PropTypes.func
};

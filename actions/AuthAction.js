import { instance as axios } from '../utils/interceptor';
import {
  IS_AUTH, AUTH_ERROR, REGISTRATION, REGISTRATION_ERROR
} from './types';

export const isAuth = id => (dispatch) => {
  axios.get(`inventory/${id}/unmarked`)
    .then((res) => {
      dispatch({
        type: IS_AUTH,
        payload: res.data.AllObjectsWithIds
      });
    })
    .catch((err) => {
      dispatch({
        type: AUTH_ERROR,
        payload: err.response
      });
    });
};

export const registrationAction = code => (dispatch) => {
  return axios.post(`auth`,code)
    .then((res) => {
      dispatch({
        type: REGISTRATION,
        payload: res.data
      });
    })
    .catch((err) => {
      dispatch({
        type: REGISTRATION_ERROR,
        payload: err.response
      });
      return Promise.reject(err);
    });
};


